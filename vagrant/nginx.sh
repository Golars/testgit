#!/usr/bin/env bash
apt-add-repository ppa:nginx/stable -y
apt-get -y update
apt-get -y install nginx
aptitude install -q -y -f mc

cp fastcgi.conf.dpkg-new fastcgi.conf

sudo rm /etc/nginx/sites-available/default
sudo touch /etc/nginx/sites-available/default

sudo cat >> /etc/nginx/sites-available/default <<'EOF'
server {
    charset utf-8;
    client_max_body_size 128M;

    listen 80;

    server_name streety.loc;
    root        /var/www/vergo;
    index       index.php index.html;

    access_log  /var/log/nginx/access.log;
    error_log   /var/log/nginx/error.log;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location /app {
        root /var/www/vergo/storage;
        access_log off;
        try_files $uri /images/no-image.jpg;
    }

    location ~ \.php$ {
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi.conf;
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
    }

    location ~ /\.(ht|svn|git) {
        deny all;
    }
}
server {
    charset utf-8;
    client_max_body_size 128M;

    listen 80;

    server_name files.streety.loc;
    root        /var/www/vergo/storage/app;
    index       index.html;

    access_log  /var/log/nginx/access_files.log;
    error_log   /var/log/nginx/error_files.log;

    location / {
        try_files $uri $uri/ $uri.html =404;
    }

    location ~ /\.(ht|svn|git) {
        deny all;
    }
}
EOF

sudo touch /etc/nginx/fastcgi_params

sudo service nginx restart

sudo service php7.0-fpm restart